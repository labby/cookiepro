<?php

/**
 * @module          CookiePro
 * @author          cms-lab
 * @copyright       2019-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/cookiepro/license.php
 * @license_terms   please see license
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include ".SEC_FILE."!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

// mod_cookiepro
$table_fields="
	  `id` INT NOT NULL AUTO_INCREMENT,
	  `cmp` INT NOT NULL DEFAULT -1,	
	  `consent_code` VARCHAR(512) NOT NULL DEFAULT '',	  
	  `active` INT NOT NULL DEFAULT 1,
	  PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_cookiepro", $table_fields);

// insert some default values
$field_values="
(NULL, -1, '', 1)
";
LEPTON_handle::insert_values("mod_cookiepro", $field_values);

// mod_cookiepro_cmp
$table_fields="
	  `cmp_id` INT NOT NULL AUTO_INCREMENT,
	  `cmp_name` VARCHAR(32) NOT NULL DEFAULT '-1',	
	  `cmp_link` VARCHAR(512) NOT NULL DEFAULT '',	  
	  `active` INT NOT NULL DEFAULT 0,
	  PRIMARY KEY ( `cmp_id` )
	";
LEPTON_handle::install_table("mod_cookiepro_cmp", $table_fields);

// insert some default values
$field_values="
(1, 'osano', 'https://www.osano.com', 0),
(2, 'consentmanager', 'https://www.consentmanager.net', 0),
(3, 'cookiebot', 'https://www.cookiebot.com', 0)
";
LEPTON_handle::insert_values("mod_cookiepro_cmp", $field_values);


// import default droplets
LEPTON_handle::install_droplets('cookiepro', 'droplet_CookiePro');
