<?php

/**
 * @module          CookiePro
 * @author          cms-lab
 * @copyright       2019-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/cookiepro/license.php
 * @license_terms   please see license
 *
 */
 

class cookiepro_frontend extends LEPTON_abstract_frontend
{
	public int $cmp = 0;
	public string $consent_code = "";
	
	public object|null $oTwig = null;
	public LEPTON_database $database;
	public static $instance;

	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();
		$this->cmp = $this->database->get_one("SELECT cmp FROM ".TABLE_PREFIX."mod_cookiepro ");
		$this->consent_code = $this->database->get_one("SELECT consent_code FROM ".TABLE_PREFIX."mod_cookiepro ");
		$this->oTwig = lib_twig_box::getInstance();
		$this->oTwig->registerModule('cookiepro');
	}

public function build_tags()
	{
		if(file_exists(LEPTON_PATH.'/modules/cookiepro/templates/output_custom.lte'))
		{
			$template = '@cookiepro/output_custom.lte';
		}
		else
		{
			$template = '@cookiepro/output.lte';
		}
		
		// data for twig template engine	
		$data = [
			'oCPFE'			=> $this
        ];

		// get the template-engine	
		return $this->oTwig->render( 
			$template,		//	template-filename
			$data			//	template-data
		);
	}
} // end of class
