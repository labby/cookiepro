<?php

/**
 * @module          CookiePro
 * @author          cms-lab
 * @copyright       2019-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/cookiepro/license.php
 * @license_terms   please see license
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include ".SEC_FILE."!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$MOD_COOKIEPRO = [
	'action'			=> "Aktion",
	'active'			=> "Aktiv",
	'choose_cmp'		=> "CMP auswählen",
	'cmp_code'			=> "Consent Code",
	'cmp_id'			=> "Consent Id",
	'edit_cmp'			=> "CMP Code editieren",
	'info'				=> "Addon Info",
	'link'				=> "CMP Homepage",	
	'list'				=> "Alle CMP listen",
	'list_head'			=> "Achtung:",
	'list_text'			=> "Es kann nur ein CMP aktiv sein!",
	'name'				=> "CMP Name",

	//	messages
	'record_deleted'	=> "Datensatz wurde gelöscht",	
	'record_saved'		=> "Datensatz wurde gespeichert"	
];
