### CookiePro
============

Integration of various Consent Management Provider (CMP) into [LEPTON CMS][1].<br />
You can find here a list of [supported CMPs][3]

#### Requirements

* [LEPTON CMS][1], Version >= see precheck.php


#### Installation

* download latest [.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### Notice

After installing addon you are done. <br />
Go to "Admintools" in the backend and use it!<br />
Keep in mind, that you have to register at your favorite CPM.

For further informations please read [the readme file][3]


[1]: https://lepton-cms.org "LEPTON CMS"
[2]: http://www.lepton-cms.com/lepador/admintools/cookiepro.php
[3]: https://cms-lab.com/_documentation/cookiepro.php "supported CMPs"